let path = require('path');
let { makeDir, createFiles,deleteFiles } = require('../problem1.js');

let dir =path.join(__dirname,'../JSON-temp');
let createFilesCount =5;
makeDir(dir,()=>{
  createFiles(dir,createFilesCount,()=>{
    deleteFiles(dir);
  })
});

