let path = require('path');
let {
  readGivenFile,
  convertToUpperCase,
  convertToLowerCaseAndSplit,
  sorting,
  readAndDelete,
} = require("../problem2.js");

let textFilePath = path.join(__dirname,"../lipsum.txt");

readGivenFile(textFilePath, (text) => {
  return convertToUpperCase(text, (UpperCaseContentPath) => {
    return convertToLowerCaseAndSplit(UpperCaseContentPath, (lowerCasedSentencesPath) => {
      return sorting(lowerCasedSentencesPath, (fileNamesPath) => {
        return readAndDelete(fileNamesPath);
      });
    });
  });
});