let fs = require("fs");
let path = require('path');
// 1.Reading File
let readGivenFile = (textFilePath, cb) => {
  fs.readFile(textFilePath, "utf8", (error, text) => {
     {
      if (error) throw err;
    }
    return cb(text);
  });
}; 
// 2. Convert the content to uppercase & 
let convertToUpperCase = (text, cb) => {
  let upperCaseContent = text.toUpperCase();
  console.log(`${upperCaseContent}`);
  
  // write to a new file.
  fs.writeFile(path.join(__dirname,"../LIPSUM.txt"),upperCaseContent,(err) => {
      if (err) throw err;
    });
// store the name of the new file in filenames.txt
  fs.writeFile(path.join(__dirname,"../filenames.txt"),"LIPSUM.txt",(err) => {
      if (err) throw err;
    });
  let upperCaseContentPath =path.join(__dirname,"../LIPSUM.txt");
  return cb(upperCaseContentPath);
};
// 3. read the new file and convert to lower case
let convertToLowerCaseAndSplit = (upperCaseContentPath, cb) => {
  // read the new file
  fs.readFile(upperCaseContentPath, "utf8", (error, text) => {
    if (error) {
      throw error;
      return;
    }
   //  convert it to lower case and split the contents into sentences.Then write to a new file.
    let lowerCaseText = text.toLowerCase().toString().split(".").join(".");
    fs.writeFile(path.join(__dirname,"../lowerCasedSentences.txt"),lowerCaseText,(err) => {
        if (err) throw err;
      }
    );
    //Store the name of the new file in filenames.txt
    fs.appendFile(path.join(__dirname,"../filenames.txt"),"\nlowerCasedSentences.txt",(err) => {
        if (err) throw err;
      }
    );
    console.log("lowerCasedSentences.txt Created");
    let lowerCasedSentencesPath =path.join(__dirname,"../lowerCasedSentences.txt");
    cb(lowerCasedSentencesPath);
  });
};

// 4. Read the new files,sort the content  
let sorting = (lowerCasedSentencesPath, cb) => {
  fs.readFile(lowerCasedSentencesPath, "utf8", (error, text) => {
    if (error) {
      console.log(error);
      return;
    }
//sort the content and write it  to a new file.
    let sortedText = text.toString().split(".").sort().join(".");
    fs.writeFile(path.join(__dirname,"../sortedText.txt"),sortedText,(err) => {
        if (err) throw err;
      }
    );
// store the name of the new file  in filenames.txt.
    fs.appendFile(path.join(__dirname,"../filenames.txt"),"\nsortedText.txt",(err) => {
        if (err) throw err;
      }
    );
    console.log("sortedText.txt Created");
    let fileNamesPath =path.join(__dirname,"../filenames.txt");
    return cb(fileNamesPath);
  });
};
// 5.  Read the contents of the filenames.txt and delete all the new files mentioned in that list.
let readAndDelete = (fileNamesPath) => {
  fs.readFile(fileNamesPath, "utf8", (err, fileNames) => {
    if (err) {
      console.log(err);return;
    }
    fileNames.split("\n").map((fileName) => {
      console.log(`${fileName}`);
      fs.unlink(path.join(__dirname,`../${fileName}`),(err) => {
          if (err) {
            throw err;
          }
        }
      );
    });
  });
};

module.exports = {
  readGivenFile,
  convertToUpperCase,
  convertToLowerCaseAndSplit,
  sorting,
  readAndDelete,
};